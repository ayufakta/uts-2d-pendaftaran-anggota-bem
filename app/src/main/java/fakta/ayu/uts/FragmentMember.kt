package fakta.ayu.uts

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_member.*
import kotlinx.android.synthetic.main.frag_data_member.view.*

class FragmentMember : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnDelete ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnUpdate ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idMember : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()

        v = inflater.inflate(R.layout.frag_data_member,container,false)
        v.btnUpdate.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnDelete.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsMember.setOnItemClickListener(itemClick)

        return v
    }

    fun showDataMember(){
        val cursor : Cursor = db.query("member", arrayOf("nama_member", "nama_alamat", "nama_prodi" , "id_member as _id"),
            null, null, null, null, "nama_member, nama_alamat, nama_prodi asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_member,cursor,
            arrayOf("_id", "nama_member", "nama_alamat","nama_prodi"), intArrayOf(R.id.txIdMember, R.id.txNamaMember, R.id.txAlamatMember, R.id.txProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)

        v.lsMember.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataMember()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idMember = c.getString(c.getColumnIndex("_id"))
        v.edNama.setText(c.getString(c.getColumnIndex("nama_member")))
        v.edAlamat.setText(c.getString(c.getColumnIndex("nama_alamat")))
    }

    fun insertDataMember(namaMember : String, namaAlamat : String, namaprodi : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_member",namaMember)
        cv.put("nama_alamat",namaAlamat)
        cv.put("nama_prodi",namaprodi)
        db.insert("member", null,cv)
        showDataMember()
    }

    fun updateDataMember(namaMember: String, idMember: String, namaAlamat : String, namaprodi : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_member",namaMember)
        cv.put("nama_alamat",namaAlamat)
        cv.put("nama_prodi",namaprodi)
        db.update("member", cv, "id_member = $idMember", null)
        showDataMember()
    }

    fun deleteDataMember(idMember: String){
        db.delete("member","id_member = $idMember", null)
        showDataMember()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var id: Int = radioGroup.checkedRadioButtonId
        if(id!=-1){
            val radio: RadioButton = v.findViewById(id)
            insertDataMember(v.edNama.text.toString(), v.edAlamat.text.toString(), radio.text.toString())

        }
        v.edNama.setText("")
        v.edAlamat.setText("")
        v.radioGroup.clearCheck()
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var id: Int = radioGroup.checkedRadioButtonId
        if(id!=-1) {
            val radi: RadioButton = v.findViewById(id)
            updateDataMember(v.edNama.text.toString(),idMember, v.edAlamat.text.toString(), radi.text.toString())
        }
        v.edNama.setText("")
        v.edAlamat.setText("")
        v.radioGroup.clearCheck()
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMember(idMember)
        v.edNama.setText("")
        v.edAlamat.setText("")
    }
}