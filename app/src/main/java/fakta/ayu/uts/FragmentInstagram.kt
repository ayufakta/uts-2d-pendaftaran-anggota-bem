package fakta.ayu.uts

import android.app.AlertDialog
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_instagram.view.*

class FragmentInstagram : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.buttoninstagram -> {
                val uris = Uri.parse("https://www.instagram.com/bempsdkukediri/")
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setData(uris)
                startActivity(intent)
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var db: SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var v: View
    lateinit var builder: AlertDialog.Builder
    var idMember: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()

        v = inflater.inflate(R.layout.frag_data_instagram, container, false)
        v.buttoninstagram.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)

        return v
    }
}