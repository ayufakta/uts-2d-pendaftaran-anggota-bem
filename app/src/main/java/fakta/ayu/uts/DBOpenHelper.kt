package fakta.ayu.uts

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context): SQLiteOpenHelper(context,DB_Name, null, DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMember = "create table member(id_member integer primary key autoincrement, nama_member text not null, nama_alamat text not null, nama_prodi text not null)"
        db?.execSQL(tMember)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "bempolinema"
        val DB_Ver = 1
    }
}